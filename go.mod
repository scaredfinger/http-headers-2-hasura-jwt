module hasura-headers-2-jwt.io/m/v2

go 1.16

require (
	github.com/dvsekhvalnov/jose2go v1.5.0
	github.com/gin-gonic/gin v1.6.3
)
