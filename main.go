package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"

	jose "github.com/dvsekhvalnov/jose2go"

	"github.com/gin-gonic/gin"
)

func handleWithReverseProxy(remote *url.URL, secret []byte) gin.HandlerFunc {
	return func(c *gin.Context) {
		user := "anonymous"
		
		x_default_user := os.Getenv("DEFAULT_USER")
		if x_default_user != "" {
			user = x_default_user
		}

		x_user_id_header := c.Request.Header.Get("x-user-id")
		if (x_user_id_header != "") {
			user = x_user_id_header
		}

		payload := fmt.Sprintf(
			`{
				"https://hasura.io/jwt/claims": { 
					"x-hasura-user-id": "%s", 
					"x-hasura-allowed-roles": ["user","admin"], 
					"x-hasura-default-role": "user"
				} 
			}`,
			user)

		token, err := jose.Sign(payload, jose.HS256, secret)

		if err != nil {
			panic(err)
		}

		proxy := httputil.NewSingleHostReverseProxy(remote)

		header := http.Header{}
		bearer := fmt.Sprintf("Bearer %s", token)
		header.Set("Authorization", bearer)

		proxy.Director = func(req *http.Request) {
			req.Header = header
			req.Host = remote.Host
			req.URL.Scheme = remote.Scheme
			req.URL.Host = remote.Host
			req.URL.Path = c.Param("path")
		}

		proxy.ServeHTTP(c.Writer, c.Request)
	}
}

func main() {
	router := gin.New()
	router.Use(gin.Logger())

	upstream := os.Getenv("UPSTREAM")
	secret := os.Getenv("SECRET")

	log.Print(upstream)

	remote, err := url.Parse(upstream)
	if err != nil {
		panic(err)
	}

	router.Any("/*path", handleWithReverseProxy(remote, []byte(secret)))

	router.Run(":80")
}
