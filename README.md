# Hasura Headers to JWT

Super minimalistic reverse proxy to convert `X-User-Id` http header into
a jwt for hasura.

Usage:

```text
docker run \
    --link your-hasura-container \
    --env UPSTREAM=your-hasura-container \
    --env SECRET=your-hasura-hs256-secret \
    scaredfinger/hasura-headers-to-jwt
```

## HTTP Request

```text

GET /path

X-User-Id: user-123
```

## Token

```javascript
{
  "https://hasura.io/jwt/claims": {
    "x-hasura-user-id": "user-123",
    "x-hasura-allowed-roles": ["user"],
    "x-hasura-default-role": "user",
  }
}
```

## TODOs

Will allow more HTTP headers.
