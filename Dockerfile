FROM golang:1.15.7-buster AS builder

RUN go get -u github.com/gin-gonic/gin
RUN go get github.com/dvsekhvalnov/jose2go
RUN go get github.com/gin-contrib/cors

WORKDIR /src

COPY *.go ./

RUN go build -o /app

FROM golang:1.15.7-buster AS final

COPY --from=builder /app /app

CMD /app